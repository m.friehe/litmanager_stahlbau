﻿using System;

namespace LiteraturManager
{
    public class DataHandling
    {
        public class sReference
        {
            public string RefKey;
            public string WhatsOld;
            public string WhatsNew;
        }

        public class sLitItem
        {
            public string Title;
            public string SubTitle;
            public string Institution;
            public List<string> Authors;
            public DateTime PublishedDate;
            public DateTime BeginDate;
            public DateTime SendInDate;
            public DateTime StatusDate;
            public string Publisher;
            public double Status;
            public string PublishedIn;
            public string LiteratureKey;
            public List<sReference> Refs;
        }

        public class sDataItem
        {
            public System.DateTime DataValue;
            public double Value;
        }
    }
}
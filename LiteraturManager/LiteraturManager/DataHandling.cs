﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace LiteraturManager
{
    public class DataHandling
    {
        public class cReference
        {
            public string RefKey { get; set; }
            public int RefIndex { get; set; }
            public string WhatsOld { get; set; }
            public string WhatsNew { get; set; }
        }

        public class cStatus
        {
            public double Percentage { get; set; }
            public DateTime Date { get; set; }
            public string Comment { get; set; }
            public Brush Color { get; set; }
        }

        public class cLitItem
        {
            public enum Sprache
            {
                Deutsch,
                Englisch
               
            }

            public int Index { get; set; }
            public string Title { get; set; }
            public string SubTitle { get; set; }
            public string Institution { get; set; }
            public List<string> Authors { get; set; }
            public string AuthorList
            {
                get
                {
                    string alist = "";
                    if (Authors != null)
                    {
                        int nAuthors = Authors.Count;
                        bool moreAuthors = false;
                        if (nAuthors > 3) { nAuthors = 3; moreAuthors = true; }
                        for (int i = 0; i < nAuthors; i++)
                        {
                            alist = alist + Authors[i];
                            if (i < nAuthors - 1) { alist = alist + ", "; }
                        }
                        if (moreAuthors) { alist = alist + "et. al."; }
                    }
                    return alist;
                }
                set { _PaperTextDescr = ""; }
            }
            public DateTime PublishedDate { get; set; }
            public string Department { get; set; }
            public Brush GetDepBrush()
            {
                Brush b = Brushes.White;
                if (this.Department == "Wind") { b = Brushes.Tomato; }
                if (this.Department == "Baudynamik/Verbundbau") { b = Brushes.LightBlue; }
                return b;
            }

            public string ResearchAim { get; set; }
            public DateTime BeginDate { get; set; }
            public DateTime SendInDate { get; set; }
            public DateTime StatusDate { get; set; }
            public string Publisher { get; set; }
            public string Type { get; set; }
            public Sprache Language { get; set; }
            public string IsiIndexed { get; set; }
            public string ScopusIndexed { get; set; }

            public Brush TypeColor 
            {
                get                
                {
                    return GetTypeBrush();
                }
                set
                {
                }
            }

            public Brush GetTypeBrush()
            {
                Brush col = Brushes.White;

                switch (Type)
                {
                    case "Journal":
                        col = Brushes.Red; break;
                    case "Konferenz":
                        col = Brushes.Green; break;
                    case "Richtlinie":
                        col = Brushes.GreenYellow; break;
                    case "Norm":
                        col = Brushes.Blue; break;
                    case "Bericht":
                        col = Brushes.Cyan; break;
                    case "Dissertation":
                        col = Brushes.Magenta; break;
                    case "Monographie":
                        col = Brushes.MediumPurple; break;
                    case "Buchkapitel":
                        col = Brushes.MediumTurquoise; break;
                    default:
                        break;
                }
                return col;
            }

            public List<cStatus> Status { get; set; }

            public string PublishedIn { get; set; }
            public string LiteratureKey { get; set; }
            public List<cReference> Refs { get; set; }

            private string _PaperTextDescr;
            public string PaperTextDescr { get { return Authors[0] + ": " + Title + " in " + PublishedIn + " (" + PublishedDate.Year.ToString() + ")"; } set { _PaperTextDescr = ""; } }

            public cLitItem()
            {
            }


        }

        public class cDataItem
        {
            public System.DateTime DataValue;
            public double Value;
        }
    }
}

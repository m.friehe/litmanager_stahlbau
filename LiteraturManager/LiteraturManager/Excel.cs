﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace LiteraturManager
{
    public static class ExcelClass
    {
        public static List<string[]> Read(string fname)
        {
            try
            {
                List<string[]> all = new List<string[]>();

                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                Excel.Range range;

                int rCnt = 0;
                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(fname, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                range = xlWorkSheet.UsedRange;
                
                object[,] values = (object[,])range.Value;
                //Gehe das ganze Zabellenblatt durch
                //for (rCnt = 3; rCnt <= range.Rows.Count; rCnt++)
                for (rCnt = 3; rCnt <= values.GetLength(0); rCnt++)
                {
                    //Hier haben wir Zugriff auf jede Zeile
                    //if ((range.Cells[rCnt, 2] as Excel.Range).Value2 != null)
                    if (values[rCnt,2] != null)
                    {
                        try
                        {
                            string[] hTemp = new string[55];
                            for (int iCol = 2; iCol <= 56; iCol++)
                            {
                                
                                //var val = (range.Cells[rCnt, iCol] as Excel.Range).Value;
                                var val = values[rCnt, iCol];
                                if (val is string)
                                    hTemp[iCol-2] = (string)val;
                                else if(val is DateTime)
                                    hTemp[iCol - 2] = ((DateTime)val).ToShortDateString();
                                else
                                    hTemp[iCol - 2] = "";
                            }
                            all.Add(hTemp);

                            //string sZelleSpalte1 = (string)(range.Cells[rCnt, 1] as Excel.Range).Value2;
                            //string sZelleSpalte2 = (string)(range.Cells[rCnt, 2] as Excel.Range).Value2;
                        }
                        catch { }
                    }
                }

                xlWorkBook.Close(true, null, null);
                xlApp.Quit();
                return all;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fehler in ReadExcel: " + ex.Message);
                return null;
            }

        }
    }
}

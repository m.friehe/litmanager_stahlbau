﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;


using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Wpf;
using OxyPlot.Series;
using Microsoft.Win32;
using System.IO;

namespace LiteraturManager
{

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Constructor
        
        public MainWindow()
        {
            InitializeComponent();
            //StatusBack1 = new SolidColorBrush(Colors.Blue);
            this.DataContext = this;
            StartGraLit();
            StartGraBar();

            //Mirko
            string g = System.AppDomain.CurrentDomain.BaseDirectory + "SourceFiles";
            WissMitarbeiters.ReadFile(g + "\\MItarbeiter.txt");
            string[] allines = File.ReadAllLines(g + "\\Arbeitsgruppen.txt");
            for (int i = 0; i < allines.Length; i++)
            {
                Arbeitsgruppen.Add(allines[i]);
            }
            InitMenu();
        }

        #endregion

        #region Menu

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
        }

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //UserControl usc = null;
            //GridMain.Children.Clear();

            //switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            //{
            //    case "ItemHome":
            //        usc = new UserControlHome();
            //        GridMain.Children.Add(usc);
            //        break;
            //    case "ItemCreate":
            //        usc = new UserControlCreate();
            //        GridMain.Children.Add(usc);
            //        break;
            //    default:
            //        break;
            //}
        }

        private void PopupBox_ToggleCheckedContentClick(object sender, RoutedEventArgs e)
        {

        }


        private void InitMenu()
        {
            for (int i = 0; i < Arbeitsgruppen.Count; i++)
            {
                Button bt = new Button
                {
                    Content = Arbeitsgruppen[i]
                };
                bt.Click += Bt_Click;

                stp_arbeitsgruppen.Children.Add(bt);
            }
            
        }

        private void Bt_Click(object sender, RoutedEventArgs e)
        {
            tbl_act_arbeitsgruppe.Text = ((Button)sender).Content.ToString();
        }

        #endregion

        #region PropsGraphLiteratur

        public PlotModel plmo_GraLit = new PlotModel()
        {
            //Title = "GrafikLiteratur",
            //TitleHorizontalAlignment = TitleHorizontalAlignment.CenteredWithinPlotArea,
            //TitleFontSize = 16,
            IsLegendVisible = false
        };

        public OxyPlot.Axes.LinearAxis xAchse = new OxyPlot.Axes.LinearAxis
        {
            Position = OxyPlot.Axes.AxisPosition.Bottom,
            Minimum = 0,
            Maximum = 1.0,
            Title = "Jahr",
            MajorGridlineColor = OxyColors.Gray,
            MajorGridlineStyle = LineStyle.Solid,
            MajorGridlineThickness = 0.8,
            MajorStep = 1,
            MinorStep = 0.25,
        };
     
        public OxyPlot.Axes.LinearAxis yAchse = new OxyPlot.Axes.LinearAxis
        {
            //Minimum = 0.0,
            //Maximum = 1.0,
            Position = OxyPlot.Axes.AxisPosition.Left,
            //Title = "ψ_{r}",
            MajorGridlineThickness = 0.8,
            MajorGridlineColor = OxyColors.Gray,
            MajorGridlineStyle = LineStyle.Solid,
            MinorGridlineColor = OxyColors.Gray,
            MinorGridlineThickness = 0.5,
            MinorGridlineStyle = LineStyle.Solid
        };

        private void StartGraLit()
        {
            plmo_GraLit.Axes.Add(xAchse);
            plmo_GraLit.Axes.Add(yAchse);
            graLiteratur_oxy.Model = plmo_GraLit;
        }

        #endregion

        #region PlotBar

        public PlotModel plmo_Bar= new PlotModel()
        {
            //Title = "GrafikLiteratur",
            //TitleHorizontalAlignment = TitleHorizontalAlignment.CenteredWithinPlotArea,
            //TitleFontSize = 16,
            IsLegendVisible = true,
            Title = "Verteilung",
            LegendPlacement = LegendPlacement.Outside,
            LegendPosition = LegendPosition.BottomCenter,
            LegendOrientation = LegendOrientation.Horizontal,
            LegendBorderThickness = 0
        };

        public OxyPlot.Axes.CategoryAxis BarAx = new OxyPlot.Axes.CategoryAxis
        {
            Position = AxisPosition.Left,
            Key = "Cat",
            ItemsSource = new[]
           {
                "Journal",
                "Konferenz",
                "Richtlinie",
                "Norm",
                "Bericht",
                "Dissertation",
                "Monographie",
                "Buchkapitel"
            },
        };

        private void StartGraBar()
        {
            plmo_Bar.Axes.Add(BarAx);
            pltStats_oxy.Model = plmo_Bar;
        }


        #endregion


        #region Props

        List<string> Arbeitsgruppen = new List<string>();
        Mitarbeiters WissMitarbeiters = new Mitarbeiters();

        #endregion

        public List<DataHandling.cLitItem> LITOriginal = new List<DataHandling.cLitItem>();
        public List<DataHandling.cLitItem> LIT = new List<DataHandling.cLitItem>();
        public DataHandling.cLitItem curLIT = new DataHandling.cLitItem();
        public List<DataHandling.cLitItem> DepLIT = new List<DataHandling.cLitItem>();
        public DataHandling.cLitItem curDepLIT = new DataHandling.cLitItem();


        public enum eFilterTyp { None, BeginDate, PublishDate, Department, Author }

        //public List<eFilterTyp> FilterSet = new List<eFilterTyp>() { eFilterTyp.None };

        public DateTime FilterStartTime;
        public DateTime FilterEndTime;

        System.Windows.Media.Brush _StatusBack1;
        public System.Windows.Media.Brush StatusBack1
        {
            get { return _StatusBack1; }

            set { _StatusBack1 = value; OnPropertyChanged("StatusBack1"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public List<string> Mitarbeiter = new List<string>() { 
            "Abeln",
        "Bigelow",
        "Brieden",
        "Citarelli",
        "Colomer",
        "Di Biase",
        "Feldmann",
        "Fontecha",
        "Geßler",
        "Heinemeyer",
        "Hoffmeister",
        "Kasper",
        "Kemper",
        "Kuhnhenne",
        "Kopp",
        "Korndörfer",
        "Kühne",
        "Pak",
        "Pinkawa",
        "Pyschny",
        "Reger",
        "Richter",
        "Schaffrath",
        "Schillo",
        "Wieschollek"
                };


        public void ExampleLit()
        {
            DataHandling.cLitItem i1 = new DataHandling.cLitItem();
            DataHandling.cLitItem i2 = new DataHandling.cLitItem();
            DataHandling.cLitItem i3 = new DataHandling.cLitItem();
            i1.Title = "Buch 1";
            i1.Authors = new List<string>() { "Kemper, F." };
            i1.BeginDate = new System.DateTime(2012, 8, 7);
            i1.PublishedDate = new System.DateTime(2014, 4, 1);
            i1.BeginDate = new System.DateTime(2013, 9, 10);
            i1.StatusDate = new System.DateTime(2013, 12, 10);
            i1.SendInDate = new System.DateTime(2013, 12, 31);
            DataHandling.cStatus stat = new DataHandling.cStatus();
            stat.Percentage = 90;
            stat.Comment = "Testkommentar";
            stat.Date = new System.DateTime(2014, 1, 1);
            i1.Status = new List<DataHandling.cStatus>();
            i1.Status.Add(stat);

            i1.Type = "Journal";
            i1.PublishedIn = "JWEIA";
            i1.Publisher = "Elsevier";
            i1.LiteratureKey = "KemJWEIA2014";
            i1.Refs = new List<DataHandling.cReference>();
            DataHandling.cReference r1 = new DataHandling.cReference();
            r1.RefKey = "KemEACWE2013";
            r1.WhatsNew = "Schadensäquivalenz mit Messdaten Kamin verglichen";
            r1.WhatsOld = "Methodik wurde übernommen";
            i1.Refs.Add(r1);
            LIT.Add(i1);

            i2.Title = "Beitrag in der EACWE Konferenz";
            i2.Authors = new List<string>() { "Kemper, F.", "Feldmann, M.", "Colomer, C." };
            i2.BeginDate = new System.DateTime(2012, 8, 7);
            i2.PublishedDate = new System.DateTime(2014, 4, 1);
            i2.BeginDate = new System.DateTime(2013, 9, 10);
            i2.StatusDate = new System.DateTime(2013, 12, 10);
            i2.SendInDate = new System.DateTime(2013, 12, 31);

            i2.PublishedIn = "JWEIA";
            i2.Publisher = "Elsevier";
            i2.Type = "Monographie";
            i2.LiteratureKey = "KemEACWE2013";
            i2.Refs = new List<DataHandling.cReference>();
            DataHandling.cReference r2 = new DataHandling.cReference();
            r2.RefKey = "KemJWEIA2014";
            r2.WhatsNew = "Schadensäquivalenz mit Messdaten Kamin verglichen";
            r2.WhatsOld = "Methodik wurde übernommen";
            i2.Refs.Add(r2);
            LIT.Add(i2);

            i3.Title = "Neue Methoden zur Energiegewinnung";
            i3.Authors = new List<string>() { "Kemper, F.", "Meyer, M." };
            i3.BeginDate = new System.DateTime(2012, 8, 7);
            i3.PublishedDate = new System.DateTime(2014, 4, 1);
            i3.BeginDate = new System.DateTime(2013, 9, 10);
            i3.StatusDate = new System.DateTime(2013, 12, 10);
            i3.SendInDate = new System.DateTime(2013, 12, 31);

            i3.PublishedIn = "JWEIA";
            i3.Publisher = "Elsevier";
            i3.Type = "Monographie";
            i3.LiteratureKey = "KemEnergie2013";
            i3.Refs = new List<DataHandling.cReference>();
            DataHandling.cReference r3 = new DataHandling.cReference();
            DataHandling.cReference r4 = new DataHandling.cReference();
            r3.RefKey = "KemJWEIA2014";
            r3.WhatsNew = "Schadensäquivalenz mit Messdaten Kamin verglichen";
            r3.WhatsOld = "Methodik wurde übernommen";
            r4.RefKey = "KemEACWE2013";
            r4.WhatsNew = "Inhaltlich komplett überarbeitet";
            r4.WhatsOld = "Layout übernommen";
            i3.Refs.Add(r4);
            i3.Refs.Add(r3);
            LIT.Add(i3);
        }

        public double ConvertDateTime(DateTime tmp)
        {
            double res = 0;

            res = tmp.Year + ((double)tmp.Month * 30.0 + (double)tmp.Day) / 365.0;

            return res;
        }

   

        private void UpdateBinding()
        {
            cmbCurPaper.DataContext = LIT;
            txtTitleAkt.DataContext = curLIT;
            txtUntertitelAKT.DataContext = curLIT;
            txtAutorenAKT.DataContext = curLIT;
            //txtInstitutAKT.DataContext = curLIT;
            txtBeginnJahrAKT.DataContext = curLIT;
            txtSendJahrAKT.DataContext = curLIT;
            txtJahrAKT.DataContext = curLIT;
            //txtVerlagAKT.DataContext = curLIT;
            txtArtAKT.DataContext = curLIT;
            txtArtColor.DataContext = curLIT;
            txtBeitraginAKT.DataContext = curLIT;
            txtStatus1.DataContext = curLIT;
            txtStatus2.DataContext = curLIT;
            txtStatus3.DataContext = curLIT;
            txtStatus4.DataContext = curLIT;
            txtStatusDatum1.DataContext = curLIT;
            txtStatusDatum2.DataContext = curLIT;
            txtStatusDatum3.DataContext = curLIT;
            txtStatusDatum4.DataContext = curLIT;

            txtAim.DataContext = curLIT;

            txtBereich.DataContext = curLIT;


            txtTitleDep.DataContext = curDepLIT;
            txtSubTitleDep.DataContext = curDepLIT;
            txtAuthorsDep.DataContext = curDepLIT;
            txtRefArtColor.DataContext = curDepLIT;
            //txtInstitutionDep.DataContext = curDepLIT;
            txtBeginnJahrDep.DataContext = curDepLIT;
            txtSendJahrDep.DataContext = curDepLIT;
            txtPublishJahrDep.DataContext = curDepLIT;
            //txtVerlagDep.DataContext = curDepLIT;
            txtArtDep.DataContext = curDepLIT;
            txtBeitraginDep.DataContext = curDepLIT;

            cmbCurPaper.DataContext = LIT;
        }

        private void UpdateTreeView()
        {
            DependencyTree.Items.Clear();
            for (int i = 0; i < DepLIT.Count; i++)
            {
                TreeViewItem newChild = new TreeViewItem();
                newChild.Header = DepLIT[i].PaperTextDescr;
                if (i == 0) { newChild.IsSelected = true; }
                DependencyTree.Items.Add(newChild);
            }

        }

        private void SetDependLiterature()
        {
            DepLIT = new List<DataHandling.cLitItem>();
            curDepLIT = new DataHandling.cLitItem();

            if (curLIT.Refs != null)
            {
                int nRefs = curLIT.Refs.Count();
                int nLits = LITOriginal.Count();
                for (int i = 0; i < nRefs; i++)
                {
                    bool foundkey = false; curLIT.Refs[i].RefIndex = -1;
                    string key = curLIT.Refs[i].RefKey;
                    for (int j = 0; j < nLits; j++)
                    {
                        if (key == LITOriginal[j].LiteratureKey)
                        {
                            //Übereinstimmung gefunden
                            foundkey = true;
                            curLIT.Refs[i].RefIndex = j;
                            DepLIT.Add(LITOriginal[j]);
                        }
                    }
                    if (foundkey == false)
                    {
                        SetNewComment("Achtung, Schlüssel nicht gefunden: [" + key + "]");
                    }
                }
            }
            UpdateTreeView();
            UpdateBinding();
        }


        public PlotModel MyModel { get; private set; }

        private void UpdateTimeLine()
        {


            //int n = 10;
            //for (double x = -10; x < 10; x += 0.01)
            //{
            //    double y = 0;
            //    for (int i = 0; i < n; i++)
            //    {
            //        int j = i * 2 + 1;
            //        y += Math.Sin(j * x) / j;
            //    }
            //    lp.Add(new DataPoint(x, y));

            //}

            //List<List<DataPoint>> AllPoints = new List<List<DataPoint>>();
            //List<DataPoint> StatusPoints = new List<DataPoint>();


            //DateTime RefPaper1 = curLIT.BeginDate;
            //DateTime RefPaper2 = curLIT.SendInDate;
            //DateTime RefPaper3 = curLIT.PublishedDate;

            //if (curLIT.Status != null)
            //{
            //    int nStats = curLIT.Status.Count();
            //    DateTime[] StatDate = new DateTime[nStats];
            //    Double[] StatProg = new double[nStats];

            //    for (int i = 0; i < nStats; i++)
            //    {
            //        StatDate[i] = curLIT.Status[i].Date;
            //        StatProg[i] = curLIT.Status[i].Percentage;
            //    }
            //}

            //List<DataPoint> RefPoints = new List<DataPoint>();
            //RefPoints.Add(OxyPlot.Axes.DateTimeAxis.CreateDataPoint(RefPaper1, 1));
            //RefPoints.Add(OxyPlot.Axes.DateTimeAxis.CreateDataPoint(RefPaper2, 1));
            //RefPoints.Add(OxyPlot.Axes.DateTimeAxis.CreateDataPoint(RefPaper3, 1));

            //StatusPoints.Add(new DataPoint(ConvertDateTime(RefPaper1), 6));
            //AllPoints.Add(RefPoints);

            //if (curLIT.Refs != null)
            //{
            //    for (int i = 0; i < curLIT.Refs.Count; i++)
            //    {
            //        if (curLIT.Refs[i].RefIndex > -1)
            //        {
            //            List<DataPoint> DefPoints = new List<DataPoint>();
            //            DateTime DefPaper1 = LITOriginal[curLIT.Refs[i].RefIndex].BeginDate;
            //            DateTime DefPaper2 = LITOriginal[curLIT.Refs[i].RefIndex].SendInDate;
            //            DateTime DefPaper3 = LITOriginal[curLIT.Refs[i].RefIndex].PublishedDate;
            //            DefPoints.Add(new DataPoint(ConvertDateTime(DefPaper1), 2 + i));
            //            DefPoints.Add(new DataPoint(ConvertDateTime(DefPaper2), 2 + i));
            //            DefPoints.Add(new DataPoint(ConvertDateTime(DefPaper3), 2 + i));
            //            AllPoints.Add(DefPoints);
            //        }
            //    }
            //}


            ////CurrentPaper
            //OxyPlot.Wpf.LineSeries ls0 = new OxyPlot.Wpf.LineSeries();
            //ls0.ItemsSource = AllPoints[0];
            //ls0.LineStyle = LineStyle.Solid;
            //ls0.Color = Color.FromRgb(255, 0, 0);
            //oxyTimeLine.Series.Add(ls0);
            //oxyTimeLine.UpdateLayout();

            ////RefPaper1
            //if (AllPoints.Count > 1)
            //{
            //    OxyPlot.Wpf.LineSeries ls1 = new OxyPlot.Wpf.LineSeries();
            //    ls1.ItemsSource = AllPoints[1];
            //    ls1.LineStyle = LineStyle.Solid;
            //    ls1.Color = Color.FromRgb(0, 255, 0);
            //    oxyTimeLine.Series.Add(ls1);
            //    oxyTimeLine.UpdateLayout();
            //}

            //return;

            //Daten der Aktuellen Veröffentlichung
            //graLiteratur.Plots.Clear();
            List<List<Point>> AllPoints = new List<List<Point>>();
            List<Point> StatusPoints = new List<Point>();


            DateTime RefPaper1 = curLIT.BeginDate;
            DateTime RefPaper2 = curLIT.SendInDate;
            DateTime RefPaper3 = curLIT.PublishedDate;



            List<Point> RefPoints = new List<Point>();
            RefPoints.Add(new Point(ConvertDateTime(RefPaper1), 1));
            RefPoints.Add(new Point(ConvertDateTime(RefPaper2), 1));
            RefPoints.Add(new Point(ConvertDateTime(RefPaper3), 1));

            StatusPoints.Add(new Point(ConvertDateTime(RefPaper1), 6));
            AllPoints.Add(RefPoints);

            if (curLIT.Refs != null)
            {
                for (int i = 0; i < curLIT.Refs.Count; i++)
                {
                    if (curLIT.Refs[i].RefIndex > -1)
                    {
                        List<Point> DefPoints = new List<Point>();
                        DateTime DefPaper1 = LITOriginal[curLIT.Refs[i].RefIndex].BeginDate;
                        DateTime DefPaper2 = LITOriginal[curLIT.Refs[i].RefIndex].SendInDate;
                        DateTime DefPaper3 = LITOriginal[curLIT.Refs[i].RefIndex].PublishedDate;
                        DefPoints.Add(new Point(ConvertDateTime(DefPaper1), 2 + i));
                        DefPoints.Add(new Point(ConvertDateTime(DefPaper2), 2 + i));
                        DefPoints.Add(new Point(ConvertDateTime(DefPaper3), 2 + i));
                        AllPoints.Add(DefPoints);
                    }
                }
            }

            int nP = AllPoints.Count;
            for (int i = 0; i < 10 - nP; i++)
            {
                AllPoints.Add(null);
            }


            if (curLIT.Status != null)
            {
                int nStats = curLIT.Status.Count();
                DateTime[] StatDate = new DateTime[nStats];
                Double[] StatProg = new double[nStats];

                for (int i = 0; i < nStats; i++)
                {
                    List<Point> tmp = new List<Point>();
                    StatDate[i] = curLIT.Status[i].Date;
                    StatProg[i] = curLIT.Status[i].Percentage;

                    //StatusPlots
                    tmp.Add(new Point(ConvertDateTime(StatDate[i]), 6.0));



                    //graLiteratur.Plots[10 + i].DataContext = curLIT.Status[i];  //Mirko
                    AllPoints.Add(tmp);
                }
            }


            //Mirko NI Graph ersetzt mit Oxyplot
            plmo_GraLit.Series.Clear();
            double minyear = 0;
            double maxyear = 1;
            double maxval = 2;
            if (AllPoints.Count != 0)
            {
                maxval = double.MinValue;
                minyear = double.MaxValue;
                maxyear = double.MinValue;
                for (int iPlot = 0; iPlot < AllPoints.Count; iPlot++)
                {
                    
                    if (AllPoints[iPlot] == null) continue;
                    maxval++;
                    OxyColor col = OxyColor.Parse("#FFFFA500");
                    if(iPlot != 0)
                        col = OxyColor.Parse("#8AACB8");
                    var htemp = new OxyPlot.Series.LineSeries() { MarkerType = MarkerType.Diamond, Title = iPlot.ToString(), StrokeThickness = 15, Color = col };
                    for (int iPoint = 0; iPoint < AllPoints[iPlot].Count; iPoint++)
                    {
                        htemp.Points.Add(new DataPoint(AllPoints[iPlot][iPoint].X, AllPoints[iPlot][iPoint].Y));
                        if (AllPoints[iPlot][iPoint].X < minyear)
                            minyear = Math.Floor(AllPoints[iPlot][iPoint].X);
                        if (AllPoints[iPlot][iPoint].X > maxyear)
                            maxyear = Math.Ceiling(AllPoints[iPlot][iPoint].X);
                        if (AllPoints[iPlot][iPoint].Y > maxval)
                            maxval = AllPoints[iPlot][iPoint].Y;
                    }
                    plmo_GraLit.Series.Add(htemp);
                }
            }

            yAchse.Minimum = -0.5;
            yAchse.Maximum = maxval + 0.5;
            xAchse.Minimum = minyear;
            xAchse.Maximum = maxyear;

            plmo_GraLit.InvalidatePlot(true);

            //graLiteratur.Plots[10].DataContext = System.Windows.Media.Brushes.Green;  //Mirko
            //graLiteratur.RenderMode = NationalInstruments.Controls.RenderMode.Vector;  //war schon




        }

        private void cmbCurPaper_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LIT != null & cmbCurPaper.SelectedIndex > -1)
            {
                curLIT = LIT[cmbCurPaper.SelectedIndex];
                UpdateBinding();
                //Create New Depend List
                SetDependLiterature();
                UpdateTimeLine();
                //txtComment.AppendText(LIT[cmbCurPaper.SelectedIndex].PaperTextDescr + "\n");
            }
        }

        private void DependencyTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            int i = 0;
            int selIndex = 0;
            object cur = DependencyTree.SelectedItem;
            foreach (var _item in DependencyTree.Items)
            {
                if (cur == _item)
                {
                    curDepLIT = DepLIT[i];
                    selIndex = i;
                }
                i += 1;
            }
            UpdateBinding();
            if (curLIT.Refs != null)
            {
                txtWhatsNew.Text = "NEU: " + curLIT.Refs[selIndex].WhatsNew;
                txtWhatsOld.Text = "ALT: " + curLIT.Refs[selIndex].WhatsOld;
            }
            else
            {
                txtWhatsNew.Text = "";
                txtWhatsOld.Text = "";
            }
        }

        private Brush GetStatusBrush(double stat)
        {
            Brush b = Brushes.White;
            if (stat <= 10) { b = Brushes.Magenta; }
            if (stat > 10 & stat <= 30) { b = Brushes.Red; }
            if (stat > 30 & stat <= 50) { b = Brushes.DarkOrange; }
            if (stat > 50 & stat <= 75) { b = Brushes.Orange; }
            if (stat > 75 & stat < 100) { b = Brushes.Yellow; }
            if (stat == 100) { b = Brushes.YellowGreen; }
            return b;
        }
        private Brush GetTypeBrush(double type)
        {
            //
            Brush b = Brushes.White;
            return b;
        }

        private void cmdImport_Click(object sender, RoutedEventArgs e)
        {
            
        }

        public class cStats
        {
            public int Values { get; set; }
            public string Descr { get; set; }
            public Brush Color { get; set; }
        }

        public List<cStats> stat { get; set; }


        private void UpdateStats()
        {
            //German
            stat = GetStatistics(DataHandling.cLitItem.Sprache.Deutsch);
            List<int[]> dgroup = new List<int[]>();
            for (int i = 0; i < 8; i++)
            {
                dgroup.Add(new int[8]);
                dgroup.Last()[i] = stat[i].Values;
            }
            //German
           List<cStats> staten = GetStatistics(DataHandling.cLitItem.Sprache.Englisch);
            List<int[]> dgroupen = new List<int[]>();
            for (int i = 0; i < 8; i++)
            {
                dgroupen.Add(new int[8]);
                dgroupen.Last()[i] = staten[i].Values;
            }

            //

            plmo_Bar.Series.Clear();
            //German
            var barseries = new OxyPlot.Series.BarSeries
            {
                ItemsSource = new List<BarItem>(new[] {
                    new BarItem { Value = Convert.ToDouble(stat[0].Values) },
                    new BarItem { Value = Convert.ToDouble(stat[1].Values) },
                    new BarItem { Value = Convert.ToDouble(stat[2].Values) },
                    new BarItem { Value = Convert.ToDouble(stat[3].Values) },
                    new BarItem { Value = Convert.ToDouble(stat[4].Values) },
                    new BarItem { Value = Convert.ToDouble(stat[5].Values) },
                    new BarItem { Value = Convert.ToDouble(stat[6].Values) },
                    new BarItem { Value = Convert.ToDouble(stat[7].Values) }
                }),
                Title = "Deutsch",
                LabelPlacement = LabelPlacement.Inside,
                LabelFormatString = "{0:0}",
                StrokeColor = OxyColors.Black,
                FillColor = OxyColors.LightGreen
            };

            var barseriesen = new OxyPlot.Series.BarSeries
            {
                ItemsSource = new List<BarItem>(new[] {
                        new BarItem { Value = Convert.ToDouble(staten[0].Values) },
                        new BarItem { Value = Convert.ToDouble(staten[1].Values) },
                        new BarItem { Value = Convert.ToDouble(staten[2].Values) },
                        new BarItem { Value = Convert.ToDouble(staten[3].Values) },
                        new BarItem { Value = Convert.ToDouble(staten[4].Values) },
                        new BarItem { Value = Convert.ToDouble(staten[5].Values) },
                        new BarItem { Value = Convert.ToDouble(staten[6].Values) },
                        new BarItem { Value = Convert.ToDouble(staten[7].Values) }
                    }),
                Title = "Englisch",
                LabelPlacement = LabelPlacement.Inside,
                LabelFormatString = "{0:0}",
                StrokeColor = OxyColors.Black,
                FillColor = OxyColors.DarkGray
            };

            plmo_Bar.Series.Add(barseries); plmo_Bar.Series.Add(barseriesen);
            plmo_Bar.InvalidatePlot(true);

            //pltStats.ToolTip="TEST";  //war schon
        }

        private List<DataHandling.cLitItem> GetFilteredLit(List<DataHandling.cLitItem> LitIn, eFilterTyp FT, DateTime starttime, DateTime endtime, string filtertext)
        {
            List<DataHandling.cLitItem> flit = new List<DataHandling.cLitItem>();


            //Beginndate
            if (FT == eFilterTyp.BeginDate)
            {
                foreach (DataHandling.cLitItem l in LitIn)
                {

                    if (l.BeginDate >= starttime && l.BeginDate <= endtime)
                    {
                        flit.Add(l);
                    }
                }
            }

            //Publishdate
            if (FT == eFilterTyp.PublishDate)
            {
                foreach (DataHandling.cLitItem l in LitIn)
                {

                    if (l.PublishedDate >= starttime && l.PublishedDate <= endtime)
                    {
                        flit.Add(l);
                    }
                }
            }

            //Department
            if (FT == eFilterTyp.Department)
            {
                foreach (DataHandling.cLitItem l in LitIn)
                {

                    if (l.Department.Contains(filtertext))
                    {
                        flit.Add(l);
                    }
                }
            }

            //Department
            if (FT == eFilterTyp.Author)
            {
                foreach (DataHandling.cLitItem l in LitIn)
                {
                    foreach (string a in l.Authors)
                    {
                        if (a.Contains(filtertext))
                        {
                            flit.Add(l);
                            break;
                        }
                    }
                }
            }

            return flit;
        }


        private Brush GetTypeColor(string Type)
        {
            Brush col = Brushes.White;

            switch (Type)
            {
                case "Journal":
                    col = Brushes.Red; break;
                case "Konferenz":
                    col = Brushes.Green; break;
                case "Richtlinie":
                    col = Brushes.GreenYellow; break;
                case "Norm":
                    col = Brushes.Blue; break;
                case "Bericht":
                    col = Brushes.Cyan; break;
                case "Dissertation":
                    col = Brushes.Magenta; break;
                case "Monographie":
                    col = Brushes.MediumPurple; break;
                case "Buchkapitel":
                    col = Brushes.MediumTurquoise; break;
                default:
                    break;
            }
            return col;
        }


        private List<cStats> GetStatistics(DataHandling.cLitItem.Sprache? spr)
        {
            List<cStats> res = new List<cStats>();
            int nRubriken = 8;
            for (int i = 0; i < nRubriken; i++)
            {
                res.Add(new cStats());
            }

            res[0].Descr = "Journal";
            res[1].Descr = "Konferenz";
            res[2].Descr = "Richtlinie";
            res[3].Descr = "Norm";
            res[4].Descr = "Bericht";
            res[5].Descr = "Dissertation";
            res[6].Descr = "Monographie";
            res[7].Descr = "Buchkapitel";

            for (int i = 0; i < nRubriken; i++)
            {
                res[i].Color = GetTypeColor(res[i].Descr);
            }

            foreach (DataHandling.cLitItem i in LIT)
            {
                if(spr != null)
                    if (i.Language != spr) continue;
                switch (i.Type)
                {
                    case "Journal":
                        res[0].Values += 1; break;
                    case "Konferenz":
                        res[1].Values += 1; break;
                    case "Richtlinie":
                        res[2].Values += 1; break;
                    case "Norm":
                        res[3].Values += 1; break;
                    case "Bericht":
                        res[4].Values += 1; break;
                    case "Dissertation":
                        res[5].Values += 1; break;
                    case "Monographie":
                        res[6].Values += 1; break;
                    case "Buchkapitel":
                        res[7].Values += 1; break;
                    default:
                        break;
                }
            }

            return res;
        }

        public struct sYears
        {
            public string Year;
            public List<DataHandling.cLitItem>  EngJournal;
            public List<DataHandling.cLitItem>  GerJournal;
            public List<DataHandling.cLitItem>  EngConference;
            public List<DataHandling.cLitItem>  GerConference;
            public List<DataHandling.cLitItem>  Others;
        }

        public struct sStatistik
        {
           public string Author;
           public List<sYears> YearWiseLiterature;
        }

        private void cmdStatistik_Click(object sender, RoutedEventArgs e)
        {
            //

            List<sStatistik> SList = new List<sStatistik>();
            eFilterTyp DateFilter = new eFilterTyp();
            eFilterTyp TextFilter = new eFilterTyp();
            DateTime s1 = new DateTime(); DateTime s2 = new DateTime();
            FilterungAus(true);
            foreach (string m in Mitarbeiter)
            {
                SetNewComment("Mitarbeiter: " + m);
                sStatistik tmpS = new sStatistik();
                tmpS.Author = m;
                List<sYears> tmpY=new List<sYears>();
   
                for (int i = 2010; i < 2020; i++)
                {
                    sYears iY = new sYears();
                    iY.Year = i.ToString();
                    DateTime.TryParse("1.1." + i.ToString(), out s1); FilterStartTime = s1;
                    DateTime.TryParse("31.12."+i.ToString(), out s2); FilterEndTime = s2;
                    Filterung(s1, s2, m, eFilterTyp.PublishDate, eFilterTyp.Author,true);


                    iY.EngConference = new List<DataHandling.cLitItem>();
                    iY.GerConference = new List<DataHandling.cLitItem>();
                    iY.EngJournal = new List<DataHandling.cLitItem>();
                    iY.GerJournal = new List<DataHandling.cLitItem>();
                    iY.Others = new List<DataHandling.cLitItem>();

                    foreach (DataHandling.cLitItem ci in LIT)
                    {
                        if (ci.Type.Contains("Journal") && ci.Language == DataHandling.cLitItem.Sprache.Englisch) { iY.EngJournal.Add(ci); }
                        if (ci.Type.Contains("Journal") && ci.Language == DataHandling.cLitItem.Sprache.Deutsch) { iY.GerJournal.Add(ci); }
                        if (ci.Type.Contains("Konferenz") && ci.Language == DataHandling.cLitItem.Sprache.Englisch) { iY.EngConference.Add(ci); }
                        if (ci.Type.Contains("Konferenz") && ci.Language == DataHandling.cLitItem.Sprache.Deutsch) { iY.GerConference.Add(ci); }
                        if (ci.Type != "Journal" && ci.Type != "Konferenz") { iY.Others.Add(ci); }
                    }
                    tmpY.Add(iY);
                }

                tmpS.YearWiseLiterature = new List<sYears>(tmpY);
                SList.Add(tmpS);
            }
            PrintStatistik(SList);
        }


        private void PrintStatistik(List<sStatistik> SList)
        {
            foreach (sStatistik s in SList)
            {
                SetNewComment("\nMitarbeiter: " + s.Author);
                SetNewComment("Jahr\tKonferenz (Eng)\tJournal (Eng)\tKonferenz (Deu)\tJournal (Deu)\tSonstige");
                foreach (sYears sy in s.YearWiseLiterature)
                {
                    SetNewComment(sy.Year + "\t" + sy.EngConference.Count + "\t" + sy.EngJournal.Count + "\t" + sy.GerConference.Count + "\t" + sy.GerJournal.Count + "\t" + sy.Others.Count);
                }
            }
        }

        private void cmdFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!datestart.SelectedDate.HasValue) return;
            if (!datesend.SelectedDate.HasValue) return;

            eFilterTyp DateFilter = new eFilterTyp();
            eFilterTyp TextFilter = new eFilterTyp();
            DateTime s1 = datestart.SelectedDate.Value; DateTime s2 = datesend.SelectedDate.Value;
            FilterStartTime = s1;
            FilterEndTime = s2;
            string FilterText = txtFilterText.Text;
            if (cmdFilter.Content.ToString() == "Filter einschalten")
            {
                if (cmbFilter1.SelectedIndex == 1) { DateFilter = eFilterTyp.PublishDate; }
                if (cmbFilter1.SelectedIndex == 2) { DateFilter = eFilterTyp.BeginDate; }
                if (cmbFilter3.SelectedIndex == 1) { TextFilter = eFilterTyp.Department; }
                if (cmbFilter3.SelectedIndex == 2) { TextFilter = eFilterTyp.Author; }
                Filterung(s1, s2, FilterText, DateFilter, TextFilter, false);
                cmdFilter.Content = "Filter ausschalten";
            }
            else
            {
                FilterungAus(false);
                cmdFilter.Content = "Filter einschalten";
            }
        }

        private void Filterung(DateTime s1, DateTime s2, string ft, eFilterTyp DateFilter, eFilterTyp TextFilter, bool isQuiet)
        {

            LIT = LITOriginal;
            //Filterung aktivieren

            if (DateFilter!=eFilterTyp.None)
            {
                LIT = GetFilteredLit(LIT, DateFilter, s1, s2, "");
            }
            if (TextFilter != eFilterTyp.None)
            {
                LIT = GetFilteredLit(LIT, TextFilter, s1, s2, ft);
            }

            if (LIT.Count > 0) { curLIT = LIT[0]; } else { if (!isQuiet) { MessageBox.Show("Keine Daten gefunden"); } }
            UpdateBinding();
            cmbCurPaper.SelectedIndex = 0;
            if (!isQuiet) { SetNewComment("Filterung der Liste: " + LIT.Count() + " / " + LITOriginal.Count()); }
            UpdateStats();

            
        }

        private void FilterungAus(bool isQuiet)
        {
            eFilterTyp FT = new eFilterTyp();
            //Filterung deaktivieren
            FT = eFilterTyp.None;
            LIT = LITOriginal;
            curLIT = LIT[0];
            //cmbFilter1.SelectedIndex = 0;
            //cmbFilter3.SelectedIndex = 0;
            UpdateBinding();
            if (!isQuiet) { SetNewComment("Filterung aufgehoben: " + LIT.Count() + " / " + LITOriginal.Count()); }
            UpdateStats();

          
        }

        private void SetNewComment(string comment)
        {
            //DateTime timestamp = System.DateTime.Now;
            //string strTimeStamp = timestamp.Hour.ToString("00") + ":" + timestamp.Minute.ToString("00") + ":" + timestamp.Second.ToString("00");
            ////txtComment.AppendText(strTimeStamp + "> " + comment + "\n");
            //txtComment.AppendText(comment + "\n");
            //txtComment.ScrollToEnd();
        }

        private void ItemHome_Selected(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Excel Files (*.xlsx)|*.xlsx|Excel Files_old (*.xls)|*.xls";
            //ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.);

            ofd.RestoreDirectory = true;
            if (ofd.ShowDialog() == false) return;
            //
            //string fname = @"d:\GitLab\LitManager_Stahlbau\LiteraturManager\LiteraturManager\File\STB-Veröffentlichungen.xlsx";
            string fname = ofd.FileName;
            List<string[]> ImportData = ExcelClass.Read(fname);


            //Prüfung der Gültigkeit

            //Verarbeitung und Aufbau der Literaturliste
            if (ImportData.Count > 3)
            {
                LIT = new List<DataHandling.cLitItem>();

                int offsetcol = -2;
                for (int i = 0; i < ImportData.Count; i++)
                {
                    DataHandling.cLitItem lItem = new DataHandling.cLitItem();
                    string[] row = ImportData[i];
                    lItem.Department = row[3 + offsetcol];
                    lItem.Title = row[4 + offsetcol];
                    lItem.SubTitle = row[5 + offsetcol];
                    lItem.Authors = new List<string>();
                    lItem.Authors.Add(row[6 + offsetcol]);
                    if (row[7 + offsetcol] != "") { lItem.Authors.Add(row[7 + offsetcol]); }
                    if (row[8 + offsetcol] != "") { lItem.Authors.Add(row[8 + offsetcol]); }
                    if (row[9 + offsetcol] != "") { lItem.Authors.Add(row[9 + offsetcol]); }
                    if (row[10 + offsetcol] != "") { lItem.Authors.Add(row[10 + offsetcol]); }
                    if (row[11 + offsetcol] != "") { lItem.Authors.Add(row[11 + offsetcol]); }

                    lItem.Type = row[12 + offsetcol];
                    lItem.PublishedIn = row[13 + offsetcol];
                    DataHandling.cLitItem.Sprache sp;
                    if (!Enum.TryParse(row[14 + offsetcol], false, out sp))
                    {
                        sp = DataHandling.cLitItem.Sprache.Deutsch;
                        SetNewComment("Sprache wurde nicht gefunden: " + lItem.Title + " (Auf deutsch gesetzt)");
                    }
                    lItem.Language = sp;
                    lItem.IsiIndexed = row[15 + offsetcol];
                    lItem.ScopusIndexed = row[16 + offsetcol];

                    lItem.ResearchAim = row[17 + offsetcol];

                    DateTime dt = new DateTime();
                    if (DateTime.TryParse(row[18 + offsetcol], out dt)) { lItem.BeginDate = dt; }
                    if (DateTime.TryParse(row[19 + offsetcol], out dt)) { lItem.SendInDate = dt; }



                    if (DateTime.TryParse(row[20 + offsetcol], out dt)) { lItem.PublishedDate = dt; }

                    if (lItem.SendInDate < lItem.BeginDate) { lItem.SendInDate = lItem.PublishedDate; }

                    if (row[21 + offsetcol] != "")
                    {
                        DataHandling.cStatus stat = new DataHandling.cStatus();
                        lItem.Status = new List<DataHandling.cStatus>();
                        stat.Percentage = Convert.ToDouble(row[21 + offsetcol]);
                        stat.Comment = row[23 + offsetcol];
                        stat.Color = GetStatusBrush(stat.Percentage);
                        if (DateTime.TryParse(row[22 + offsetcol], out dt)) { stat.Date = dt; }

                        lItem.Status.Add(stat);

                        if (row[24 + offsetcol] != "")
                        {
                            DataHandling.cStatus stat2 = new DataHandling.cStatus();
                            stat2.Percentage = Convert.ToDouble(row[24 + offsetcol]);
                            stat2.Comment = row[26 + offsetcol];
                            stat2.Color = GetStatusBrush(stat2.Percentage);
                            if (DateTime.TryParse(row[25 + offsetcol], out dt)) { stat2.Date = dt; }
                            lItem.Status.Add(stat2);

                            if (row[27 + offsetcol] != "")
                            {
                                DataHandling.cStatus stat3 = new DataHandling.cStatus();
                                stat3.Percentage = Convert.ToDouble(row[27 + offsetcol]);
                                stat3.Comment = row[29 + offsetcol];
                                stat3.Color = GetStatusBrush(stat3.Percentage);
                                if (DateTime.TryParse(row[28 + offsetcol], out dt)) { stat3.Date = dt; }
                                lItem.Status.Add(stat3);

                                if (row[30 + offsetcol] != "") //Status4
                                {
                                    DataHandling.cStatus stat4 = new DataHandling.cStatus();
                                    stat3.Percentage = Convert.ToDouble(row[30 + offsetcol]);
                                    stat3.Comment = row[32 + offsetcol];
                                    stat3.Color = GetStatusBrush(stat4.Percentage);
                                    if (DateTime.TryParse(row[31 + offsetcol], out dt)) { stat4.Date = dt; }
                                    lItem.Status.Add(stat4);
                                }

                            }
                        }
                    }
                    lItem.Publisher = ""; //Nicht in Excel Tabelle
                    lItem.LiteratureKey = row[2 + offsetcol];

                    if (row[33 + offsetcol] != "")
                    {
                        lItem.Refs = new List<DataHandling.cReference>();
                        DataHandling.cReference r = new DataHandling.cReference();
                        r.RefKey = row[33 + offsetcol];
                        r.WhatsOld = row[34 + offsetcol];
                        r.WhatsNew = row[35 + offsetcol];
                        lItem.Refs.Add(r);

                        if (row[36 + offsetcol] != "")
                        {
                            DataHandling.cReference r1 = new DataHandling.cReference();
                            r1.RefKey = row[36 + offsetcol];
                            r1.WhatsOld = row[37 + offsetcol];
                            r1.WhatsNew = row[38 + offsetcol];
                            lItem.Refs.Add(r1);

                            if (row[39 + offsetcol] != "")
                            {
                                DataHandling.cReference r2 = new DataHandling.cReference();
                                r2.RefKey = row[39 + offsetcol];
                                r2.WhatsOld = row[40 + offsetcol];
                                r2.WhatsNew = row[41 + offsetcol];
                                lItem.Refs.Add(r2);
                            }
                        }
                    }
                    LIT.Add(lItem);
                    LITOriginal = LIT;
                    curLIT = LIT[0];
                    cmbCurPaper.SelectedIndex = 0;
                    cmbCurPaper.Focus();
                    UpdateBinding();
                }
            }
            if (LIT.Count > 0)
            {
                SetNewComment("Import erfolgreich, Liste mit " + LIT.Count + " Einträgen eingelesen.");
            }
            else
            {
                SetNewComment("Import fehlgeschlagen!");
            }
            SetDependLiterature();
            UpdateTimeLine();
            UpdateStats();
        }

       
    }


}

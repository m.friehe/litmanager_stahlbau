﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturManager
{
   public class Mitarbeiter
    {
        public Mitarbeiter(string vname, string nname, string bereich)
        {
            Vorname = vname;
            Nachname = nname;
            Arbeitsgruppe = bereich;
        }

        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Arbeitsgruppe { get; set; }
    }

    public class Mitarbeiters : List<Mitarbeiter>
    {
        public void ReadFile(string fname)
        {
            string[] alllines = File.ReadAllLines(fname);
            for (int i = 0; i < alllines.Length; i++)
            {
                string[] aline = alllines[i].Split(new char[] { ';'});
                this.Add(new Mitarbeiter("", aline[0], aline[1]));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiteraturManager
{
    /// <summary>
    /// Interaktionslogik für Window1.xaml
    /// </summary>
    public partial class TimeWindow : Window
    {

        DateTime _fst;
        DateTime _fet;

        public TimeWindow(DateTime fst, DateTime fet)
        {
            InitializeComponent();
            _fst = fst;
            _fet = fet;
        }

        public Tuple<DateTime, DateTime> GetTimeSpan()
        {
            InitializeComponent();
            DateTime t1 = new DateTime();
            DateTime t2 = new DateTime();
            return Tuple.Create(t1, t2);
        }

        private void frmTime_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //check timespan
            this.DialogResult = true;
            _fst = new DateTime(2001, 12, 12);
            _fst = new DateTime(2001, 12, 12);
        }
    }
}

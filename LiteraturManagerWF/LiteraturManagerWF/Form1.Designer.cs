﻿namespace LiteraturManagerWF
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbCurPaper = new System.Windows.Forms.ComboBox();
            this.DependencyTree = new System.Windows.Forms.TreeView();
            this.txtAim = new System.Windows.Forms.TextBox();
            this.txtArtAKT = new System.Windows.Forms.TextBox();
            this.txtArtDep = new System.Windows.Forms.TextBox();
            this.txtAuthorsDep = new System.Windows.Forms.TextBox();
            this.txtAutorenAKT = new System.Windows.Forms.TextBox();
            this.txtBereich = new System.Windows.Forms.TextBox();
            this.txtTitleAkt = new System.Windows.Forms.TextBox();
            this.txtTitleDep = new System.Windows.Forms.TextBox();
            this.txtUntertitelAKT = new System.Windows.Forms.TextBox();
            this.txtVerlagAKT = new System.Windows.Forms.TextBox();
            this.txtVerlagDep = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmbCurPaper
            // 
            this.cmbCurPaper.FormattingEnabled = true;
            this.cmbCurPaper.Location = new System.Drawing.Point(12, 12);
            this.cmbCurPaper.Name = "cmbCurPaper";
            this.cmbCurPaper.Size = new System.Drawing.Size(136, 21);
            this.cmbCurPaper.TabIndex = 0;
            // 
            // DependencyTree
            // 
            this.DependencyTree.Location = new System.Drawing.Point(231, 12);
            this.DependencyTree.Name = "DependencyTree";
            this.DependencyTree.Size = new System.Drawing.Size(227, 190);
            this.DependencyTree.TabIndex = 1;
            // 
            // txtAim
            // 
            this.txtAim.Location = new System.Drawing.Point(31, 78);
            this.txtAim.Name = "txtAim";
            this.txtAim.Size = new System.Drawing.Size(83, 20);
            this.txtAim.TabIndex = 2;
            // 
            // txtArtAKT
            // 
            this.txtArtAKT.Location = new System.Drawing.Point(31, 107);
            this.txtArtAKT.Name = "txtArtAKT";
            this.txtArtAKT.Size = new System.Drawing.Size(116, 20);
            this.txtArtAKT.TabIndex = 3;
            // 
            // txtArtDep
            // 
            this.txtArtDep.Location = new System.Drawing.Point(517, 81);
            this.txtArtDep.Name = "txtArtDep";
            this.txtArtDep.Size = new System.Drawing.Size(116, 20);
            this.txtArtDep.TabIndex = 4;
            // 
            // txtAuthorsDep
            // 
            this.txtAuthorsDep.Location = new System.Drawing.Point(517, 107);
            this.txtAuthorsDep.Name = "txtAuthorsDep";
            this.txtAuthorsDep.Size = new System.Drawing.Size(116, 20);
            this.txtAuthorsDep.TabIndex = 5;
            // 
            // txtAutorenAKT
            // 
            this.txtAutorenAKT.Location = new System.Drawing.Point(22, 142);
            this.txtAutorenAKT.Name = "txtAutorenAKT";
            this.txtAutorenAKT.Size = new System.Drawing.Size(116, 20);
            this.txtAutorenAKT.TabIndex = 6;
            // 
            // txtBereich
            // 
            this.txtBereich.Location = new System.Drawing.Point(31, 182);
            this.txtBereich.Name = "txtBereich";
            this.txtBereich.Size = new System.Drawing.Size(116, 20);
            this.txtBereich.TabIndex = 7;
            // 
            // txtTitleAkt
            // 
            this.txtTitleAkt.Location = new System.Drawing.Point(31, 39);
            this.txtTitleAkt.Name = "txtTitleAkt";
            this.txtTitleAkt.Size = new System.Drawing.Size(83, 20);
            this.txtTitleAkt.TabIndex = 8;
            // 
            // txtTitleDep
            // 
            this.txtTitleDep.Location = new System.Drawing.Point(517, 26);
            this.txtTitleDep.Name = "txtTitleDep";
            this.txtTitleDep.Size = new System.Drawing.Size(83, 20);
            this.txtTitleDep.TabIndex = 9;
            // 
            // txtUntertitelAKT
            // 
            this.txtUntertitelAKT.Location = new System.Drawing.Point(76, 52);
            this.txtUntertitelAKT.Name = "txtUntertitelAKT";
            this.txtUntertitelAKT.Size = new System.Drawing.Size(83, 20);
            this.txtUntertitelAKT.TabIndex = 10;
            // 
            // txtVerlagAKT
            // 
            this.txtVerlagAKT.Location = new System.Drawing.Point(58, 208);
            this.txtVerlagAKT.Name = "txtVerlagAKT";
            this.txtVerlagAKT.Size = new System.Drawing.Size(116, 20);
            this.txtVerlagAKT.TabIndex = 11;
            // 
            // txtVerlagDep
            // 
            this.txtVerlagDep.Location = new System.Drawing.Point(517, 142);
            this.txtVerlagDep.Name = "txtVerlagDep";
            this.txtVerlagDep.Size = new System.Drawing.Size(116, 20);
            this.txtVerlagDep.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 314);
            this.Controls.Add(this.txtVerlagDep);
            this.Controls.Add(this.txtVerlagAKT);
            this.Controls.Add(this.txtUntertitelAKT);
            this.Controls.Add(this.txtTitleDep);
            this.Controls.Add(this.txtTitleAkt);
            this.Controls.Add(this.txtBereich);
            this.Controls.Add(this.txtAutorenAKT);
            this.Controls.Add(this.txtAuthorsDep);
            this.Controls.Add(this.txtArtDep);
            this.Controls.Add(this.txtArtAKT);
            this.Controls.Add(this.txtAim);
            this.Controls.Add(this.DependencyTree);
            this.Controls.Add(this.cmbCurPaper);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbCurPaper;
        private System.Windows.Forms.TreeView DependencyTree;
        private System.Windows.Forms.TextBox txtAim;
        private System.Windows.Forms.TextBox txtArtAKT;
        private System.Windows.Forms.TextBox txtArtDep;
        private System.Windows.Forms.TextBox txtAuthorsDep;
        private System.Windows.Forms.TextBox txtAutorenAKT;
        private System.Windows.Forms.TextBox txtBereich;
        private System.Windows.Forms.TextBox txtTitleAkt;
        private System.Windows.Forms.TextBox txtTitleDep;
        private System.Windows.Forms.TextBox txtUntertitelAKT;
        private System.Windows.Forms.TextBox txtVerlagAKT;
        private System.Windows.Forms.TextBox txtVerlagDep;
    }
}


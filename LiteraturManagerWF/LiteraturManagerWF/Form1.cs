﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LiteraturManagerWF
{
    public partial class Form1 : Form
    {
        public List<DataHandling.cLitItem> LITOriginal = new List<DataHandling.cLitItem>();
        public List<DataHandling.cLitItem> LIT = new List<DataHandling.cLitItem>();
        public DataHandling.cLitItem curLIT = new DataHandling.cLitItem();
        public List<DataHandling.cLitItem> DepLIT = new List<DataHandling.cLitItem>();
        public DataHandling.cLitItem curDepLIT = new DataHandling.cLitItem();

        public enum eFilterTyp { None, BeginDate, PublishDate, Department, Author }

        public List<eFilterTyp> FilterSet = new List<eFilterTyp>() { eFilterTyp.None };

        public DateTime FilterStartTime;
        public DateTime FilterEndTime;


        
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            ExampleLit();

            UpdateBinding();
        }


        public void ExampleLit()
        {
            DataHandling.cLitItem i1 = new DataHandling.cLitItem();
            DataHandling.cLitItem i2 = new DataHandling.cLitItem();
            DataHandling.cLitItem i3 = new DataHandling.cLitItem();
            i1.Title = "Buch 1";
            i1.Authors = new List<string>() { "Kemper, F." };
            i1.BeginDate = new System.DateTime(2012, 8, 7);
            i1.PublishedDate = new System.DateTime(2014, 4, 1);
            i1.BeginDate = new System.DateTime(2013, 9, 10);
            i1.StatusDate = new System.DateTime(2013, 12, 10);
            i1.SendInDate = new System.DateTime(2013, 12, 31);
            DataHandling.cStatus stat = new DataHandling.cStatus();
            stat.Percentage = 90;
            stat.Comment = "Testkommentar";
            stat.Date = new System.DateTime(2014, 1, 1);
            i1.Status = new List<DataHandling.cStatus>();
            i1.Status.Add(stat);

            i1.Type = "Journal";
            i1.PublishedIn = "JWEIA";
            i1.Publisher = "Elsevier";
            i1.LiteratureKey = "KemJWEIA2014";
            i1.Refs = new List<DataHandling.cReference>();
            DataHandling.cReference r1 = new DataHandling.cReference();
            r1.RefKey = "KemEACWE2013";
            r1.WhatsNew = "Schadensäquivalenz mit Messdaten Kamin verglichen";
            r1.WhatsOld = "Methodik wurde übernommen";
            i1.Refs.Add(r1);
            LIT.Add(i1);

            i2.Title = "Beitrag in der EACWE Konferenz";
            i2.Authors = new List<string>() { "Kemper, F.", "Feldmann, M.", "Colomer, C." };
            i2.BeginDate = new System.DateTime(2012, 8, 7);
            i2.PublishedDate = new System.DateTime(2014, 4, 1);
            i2.BeginDate = new System.DateTime(2013, 9, 10);
            i2.StatusDate = new System.DateTime(2013, 12, 10);
            i2.SendInDate = new System.DateTime(2013, 12, 31);

            i2.PublishedIn = "JWEIA";
            i2.Publisher = "Elsevier";
            i2.Type = "Monographie";
            i2.LiteratureKey = "KemEACWE2013";
            i2.Refs = new List<DataHandling.cReference>();
            DataHandling.cReference r2 = new DataHandling.cReference();
            r2.RefKey = "KemJWEIA2014";
            r2.WhatsNew = "Schadensäquivalenz mit Messdaten Kamin verglichen";
            r2.WhatsOld = "Methodik wurde übernommen";
            i2.Refs.Add(r2);
            LIT.Add(i2);

            i3.Title = "Neue Methoden zur Energiegewinnung";
            i3.Authors = new List<string>() { "Kemper, F.", "Meyer, M." };
            i3.BeginDate = new System.DateTime(2012, 8, 7);
            i3.PublishedDate = new System.DateTime(2014, 4, 1);
            i3.BeginDate = new System.DateTime(2013, 9, 10);
            i3.StatusDate = new System.DateTime(2013, 12, 10);
            i3.SendInDate = new System.DateTime(2013, 12, 31);

            i3.PublishedIn = "JWEIA";
            i3.Publisher = "Elsevier";
            i3.Type = "Monographie";
            i3.LiteratureKey = "KemEnergie2013";
            i3.Refs = new List<DataHandling.cReference>();
            DataHandling.cReference r3 = new DataHandling.cReference();
            DataHandling.cReference r4 = new DataHandling.cReference();
            r3.RefKey = "KemJWEIA2014";
            r3.WhatsNew = "Schadensäquivalenz mit Messdaten Kamin verglichen";
            r3.WhatsOld = "Methodik wurde übernommen";
            r4.RefKey = "KemEACWE2013";
            r4.WhatsNew = "Inhaltlich komplett überarbeitet";
            r4.WhatsOld = "Layout übernommen";
            i3.Refs.Add(r4);
            i3.Refs.Add(r3);
            LIT.Add(i3);
            curLIT = LIT[0];
        }

        public double ConvertDateTime(DateTime tmp)
        {
            double res = 0;

            res = tmp.Year + ((double)tmp.Month * 30.0 + (double)tmp.Day) / 365.0;

            return res;
        }

        private void UpdateBinding()
        {
            cmbCurPaper.DataSource = LIT;cmbCurPaper.DisplayMember = "PaperTextDescr";

            txtTitleAkt.DataBindings.Add("Title",curLIT,"");
            //txtUntertitelAKT.DataContext = curLIT;
            //txtAutorenAKT.DataContext = curLIT;
            //txtBeginnJahrAKT.DataContext = curLIT;
            //txtSendJahrAKT.DataContext = curLIT;
            //txtJahrAKT.DataContext = curLIT;
            //txtVerlagAKT.DataContext = curLIT;
            //txtArtAKT.DataContext = curLIT;
            //txtBeitraginAKT.DataContext = curLIT;
            //txtStatus1.DataContext = curLIT;
            //txtStatus2.DataContext = curLIT;
            //txtStatus3.DataContext = curLIT;
            //txtStatusDatum1.DataContext = curLIT;
            //txtStatusDatum2.DataContext = curLIT;
            //txtStatusDatum3.DataContext = curLIT;
            //txtAim.DataContext = curLIT;

            //txtBereich.DataContext = curLIT;


            //txtTitleDep.DataContext = curDepLIT;
            //txtSubTitleDep.DataContext = curDepLIT;
            //txtAuthorsDep.DataContext = curDepLIT;
            //txtBeginnJahrDep.DataContext = curDepLIT;
            //txtSendJahrDep.DataContext = curDepLIT;
            //txtPublishJahrDep.DataContext = curDepLIT;
            //txtVerlagDep.DataContext = curDepLIT;
            //txtArtDep.DataContext = curDepLIT;
            //txtBeitraginDep.DataContext = curDepLIT;

            //cmbCurPaper.DataSource = LIT;
        }

        private void UpdateTreeView()
        {
            //DependencyTree.Items.Clear();
            //for (int i = 0; i < DepLIT.Count; i++)
            //{
            //    TreeViewItem newChild = new TreeViewItem();
            //    newChild.Header = DepLIT[i].PaperTextDescr;
            //    if (i == 0) { newChild.IsSelected = true; }
            //    DependencyTree.Items.Add(newChild);
            //}

        }
    }
}
